/*Ejercicio 1*/
/*uso de boton para cambio de color*/
$(document).on("click", "#poner-rojo", function () {
    $("div.circulo").addClass("circulo-rojo");
});
$(document).on("click", "#quitar-rojo", function () {
    $("div.circulo").removeClass("circulo-rojo");
});


/*copiar texto*/
$(document).on("click", "#boton-copiar", function () {
    var textoOriginal = $('#original').val()
    $('#copia').val(textoOriginal);
    $('.alerta').text(textoOriginal);
});

$(document).on("click", "#boton-añadir", function () {
    var textoOriginal = $('#añadir').val()
    $('.alerta').append(textoOriginal);
});

$(document).on("click", "#boton-sumar", function () {
    var valorActual = $('#input_numero').val()
    if (valorActual < 10) {
        $('#input_numero').val($('#input_numero').val() * 1 + 1)
        $('#boton-restar').attr("disabled", false);
    } else {
        $('#boton-sumar').attr("disabled", true);
    }
});
$(document).on("click", "#boton-restar", function () {
    var valorActual = $('#input_numero').val()
    if (valorActual > 0) {
        $('#input_numero').val($('#input_numero').val() * 1 - 1)
        $('#boton-sumar').attr("disabled", false);
    } else {
        $('#boton-restar').attr("disabled", true);
    }
});

$(document).on("click", "#num_aleatorio", function () {
    $('#cir1').text(Math.floor(Math.random() * 50))
    $('#cir2').text(Math.floor(Math.random() * 50))
    $('#cir3').text(Math.floor(Math.random() * 50))
});

$(document).on("click", "#siguiente", function () {
    $('#num1').val($('#num1').val() * 1 + 1)
    $('#num2').val($('#num2').val() * 1 + 1)
    $('#num3').val($('#num3').val() * 1 + 1)
});

/*VALIDACIÓN DE FORMULARIO*/


$(document).on("submit", "#formulario1", function (evento) {

    var valorActual_nombre = $('input[name="nombre"]').val();
    var valorActual_date = $('input[name="date"]').val();
    var valorActual_email = $('input[name="email"]').val();
    var password = $('input[name="password"]').val();
    var password2= $('input[name="password2"]').val();
    var validacio_gene = false; 

   /* if (valorActual_nombre) {
        valorActual_nombre.addClass("is-valid");
    } else {
        valorActual_nombre.addClass("is-invalid");
        validacio_gene = false;
    }
    if (valorActual_date) {
        valorActual_date.addClass("is-valid");
    } else {
        valorActual_date.addClass("is-invalid");
        validacio_gene = false;
    }/*
    if (valorActual_email == (String.match(/@/))) {
        valorActual_email.addClass("is-valid");
    } else {
        valorActual_email.addClass("is-invalid");
        validacio_gene = false;
    }*/
    if (password && password === password2) {
        $('input[name="password"]').addClass("is-valid");
        $('input[name="password"]').removeClass("is-valid");
    } else {
        $('input[name="password"]').addClass("is-valid");
        $('input[name="password"]').removeClass("is-valid");
        validacio_gene = false;
    }
    if (validacio_gene == true) {
        $("#formulario1").submit();
    } else {
        evento.preventDefault();
    }
});